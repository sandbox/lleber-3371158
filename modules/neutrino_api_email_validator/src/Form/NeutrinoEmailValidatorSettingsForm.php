<?php

namespace Drupal\neutrino_api_email_validator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for the neutrino email validator submodule.
 */
class NeutrinoEmailValidatorSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['neutrino_api_email_validator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neutrino_api_email_validator';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('neutrino_api_email_validator.settings');

    $form['enable_flood'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable flood protection'),
      '#description' => $this->t('Flood protection is highly recommended.'),
      '#default_value' => $config->get('enable_flood'),
    ];

    $form['flood'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Flood settings'),
      '#description' => $this->t('For example, a threshold of 5 and a window of 60 means that an individual user may only request 5 email validations per 60 seconds.'),
      '#states' => [
        'visible' => [
          '[name="enable_flood"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['flood']['flood_threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold'),
      '#description' => $this->t('This is the maximum number of email validations per window per IP.'),
      '#default_value' => $config->get('flood_threshold'),
      '#step' => 1,
      '#min' => 1,
    ];
    $form['flood']['flood_window'] = [
      '#type' => 'number',
      '#title' => $this->t('Window'),
      '#description' => $this->t('This is the number of seconds that email validation attempts are recorded for.'),
      '#default_value' => $config->get('flood_window'),
      '#step' => 1,
      '#min' => 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('neutrino_api_email_validator.settings');
    $config->set('enable_flood', $form_state->getValue('enable_flood'));
    $config->set('flood_window', $form_state->getValue('flood_window'));
    $config->set('flood_threshold', $form_state->getValue('flood_threshold'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
