<?php

namespace Drupal\neutrino_api_email_validator;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Egulias\EmailValidator\Validation\EmailValidation;
use NeutrinoAPI\NeutrinoAPIClient;

/**
 * A decorator that wraps around the email validator service.
 *
 * This service taps the Neutrino API in order to validate email addresses.
 */
class NeutrinoEmailValidator implements NeutrinoEmailValidatorInterface {

  /**
   * The decorated service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $originalService;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The cache factory service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The client factory service.
   *
   * @var \NeutrinoAPI\NeutrinoAPIClient
   */
  protected $client;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Creates an email validator service instance.
   *
   * @param \Drupal\Component\Utility\EmailValidatorInterface $original_service
   *   The service being decorated.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \NeutrinoAPI\NeutrinoAPIClient $client
   *   The client factory service.
   */
  public function __construct(EmailValidatorInterface $original_service, CacheBackendInterface $cache, TimeInterface $time, LoggerChannelInterface $logger, NeutrinoAPIClient $client) {
    $this->originalService = $original_service;
    $this->cache = $cache;
    $this->time = $time;
    $this->logger = $logger;
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllValidationMetadata($email) {

    $validation_data = $this->cache->get($email);
    if ($validation_data === FALSE) {
      $response = $this->client->emailValidate([
        'email' => $email,
        'fix-typos' => TRUE,
      ]);
      if ($response->isOK()) {
        $validation_data = $response->getData();
        $this->cache->set($email, $validation_data, $this->time->getCurrentTime() + 60);
      }
      else {
        // Fall back to the decorated service if anything goes wrong.
        $validation_data = [
          'valid' => $this->originalService->isValid($email),
        ];
        $this->logger->error($response->getErrorMessage());
      }
    }
    else {
      $validation_data = $validation_data->data;
    }

    return $validation_data ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function isValid($email, EmailValidation $email_validation = NULL) {
    $is_valid = FALSE;
    if ($this->originalService->isValid($email)) {
      $metadata = $this->getAllValidationMetadata($email);
      $is_valid = $metadata['valid'];
    }
    return $is_valid;
  }

}
