<?php

namespace Drupal\neutrino_api_email_validator\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

/**
 * Provides a REST resource for email validation.
 *
 * @RestResource(
 *   id = "neutrino_api_email_validator",
 *   label = @Translation("Neutrino email validator"),
 *   uri_paths = {
 *     "canonical" = "/neutrino-api/email-validate"
 *   }
 * )
 */
class NeutrinoEmailValidator extends ResourceBase {

  /**
   * The neutrino email validator service.
   *
   * @var \Drupal\neutrino_api_email_validator\NeutrinoEmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The request stack service.
   *
   * @var \Drupal\Core\Http\RequestStack
   */
  protected $requestStack;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->emailValidator = $container->get('email.validator');
    $instance->requestStack = $container->get('request_stack');
    $instance->flood = $container->get('flood');
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * Ensures that remote users can't be overly disruptive.
   *
   * @param string $ip
   *   The IP address of the remote user.
   * @param int $window
   *   The length of the flood window.
   * @param int $threshold
   *   The flood threshold.
   */
  protected function checkFlood($ip, $window, $threshold) {
    $is_allowed = $this->flood->isAllowed(
      'neutrino_api_email_validator',
      $threshold,
      $window,
      $ip
    );

    if (!$is_allowed) {
      throw new TooManyRequestsHttpException(
        $window,
        $this->t('Too many requests have been received.')
      );
    }

    $this->flood->register(
      'neutrino_api_email_validator',
      $window,
      $ip
    );
  }

  /**
   * Responds to email validation GET requests.
   *
   * One would typically expect validation requests to be POST, right? Well,
   * GET was chosen for two reasons.  First, GET requests are cacheable, so
   * in performance-critical applications, such as clientside validation
   * integrations, caching the metadata at the CDN layer is very attractive.
   * Second, the underlying API call to Neutrino also uses GET.  Speed at
   * every layer!
   */
  public function get() {
    $request = $this->requestStack->getCurrentRequest();

    // This should never happen unless this code is invoked via CLI.
    if (!$request) {
      throw new BadRequestHttpException();
    }

    $config = $this->configFactory->get('neutrino_api_email_validator.settings');
    // Optional security layer to prevent denial of service attacks.
    if ($config->get('enable_flood')) {
      $this->checkFlood(
        $request->getClientIp(),
        $config->get('flood_window'),
        $config->get('flood_threshold')
      );
    }

    // Basic input validation.
    $email = $request->query->get('email');
    if (!$email) {
      throw new BadRequestHttpException('Email parameter is required');
    }
    $response = new ResourceResponse(
      $this->emailValidator->getAllValidationMetadata($email)
    );

    $response->getCacheableMetadata()->addCacheContexts(['url.query_args:email']);
    return $response;
  }

}
