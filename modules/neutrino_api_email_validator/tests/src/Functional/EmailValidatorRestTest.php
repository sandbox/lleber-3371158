<?php

namespace Drupal\Tests\neutrino_api_email_validator\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Test cases for the neutrino api email validator rest proxy service.
 *
 * @group neutrino_api_email_validator
 */
class EmailValidatorRestTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'neutrino_api',
    'neutrino_api_test',
    'neutrino_api_email_validator',
    'neutrino_api_email_validator_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\user\RoleInterface $anonymous_role */
    $anonymous_role = Role::load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('restful get neutrino_api_email_validator');
    $anonymous_role->save();
  }

  /**
   * Test case for the rest resource.
   */
  public function testRestResource() {
    $assert = $this->assertSession();

    // Requests without an email parameter should fail.
    $this->drupalGet('/neutrino-api/email-validate');
    $assert->statusCodeEquals(400);
    $assert->responseContains('{"message":"Email parameter is required"}');

    // Requests with an empty email parameter should fail.
    $this->drupalGet('/neutrino-api/email-validate', [
      'query' => [
        'email' => '',
      ],
    ]);
    $assert->statusCodeEquals(400);
    $assert->responseContains('{"message":"Email parameter is required"}');

    // Submit a few valid email addresses.
    for ($i = 0; $i < 3; ++$i) {
      $this->drupalGet('/neutrino-api/email-validate', [
        'query' => [
          'email' => "test-{$i}@example.com",
        ],
      ]);
      $assert->statusCodeEquals(200);
    }

    // The sixth request should return HTTP/429 due to flood settings.
    $this->drupalGet('/neutrino-api/email-validate', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    $assert->statusCodeEquals(429);
  }

}
