<?php

namespace Drupal\Tests\neutrino_api_email_validator\Kernel;

use Drupal\KernelTests\KernelTestBase;
use NeutrinoAPI\APIResponse;

/**
 * Test cases for the Neutrino email validator service.
 *
 * @group neutrino_api_email_validator
 */
class EmailValidatorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'neutrino_api',
    'neutrino_api_test',
    'neutrino_api_email_validator',
  ];

  /**
   * Known valid data for the neutrino validator.
   */
  protected const VALID_RESPONSE_DATA = [
    'valid' => TRUE,
    'provider' => 'gmail.com',
    'typos-fixed' => FALSE,
    'domain-error' => FALSE,
    'domain' => 'gmail.com',
    'is-freemail' => TRUE,
    'syntax-error' => FALSE,
    'email' => 'someone@gmail.com',
    'is-disposable' => FALSE,
    'is-personal' => TRUE,
  ];

  /**
   * Known invalid data for the neutrino validator.
   */
  protected const INVALID_RESPONSE_DATA = [
    'valid' => FALSE,
    'provider' => '',
    'typos-fixed' => FALSE,
    'domain-error' => FALSE,
    'domain' => '',
    'is-freemail' => TRUE,
    'syntax-error' => TRUE,
    'email' => 'invalid-syntax-test',
    'is-disposable' => FALSE,
    'is-personal' => TRUE,
  ];

  /**
   * Known valid data for the fallback validator.
   */
  protected const FALLBACK_VALID_DATA = [
    'valid' => TRUE,
  ];

  /**
   * Known invalid data for the fallback validator.
   */
  protected const FALLBACK_INVALID_DATA = [
    'valid' => FALSE,
  ];

  /**
   * The email validator service.
   *
   * @var \Drupal\neutrino_api_email_validator\NeutrinoEmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The mock neutrino client.
   *
   * @var \Drupal\neutrino_api_test\MockNeutrinoClient
   */
  protected $mockClient;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->emailValidator = \Drupal::service('email.validator');
    $this->mockClient = \Drupal::service('neutrino_api.client');
  }

  /**
   * Test case for a valid email address.
   */
  public function testEmailValidatorValid() {

    $valid_response = APIResponse::ofData(
      200,
      'json',
      static::VALID_RESPONSE_DATA
    );
    $this->mockClient->setApiResponse($valid_response);

    static::assertTrue($this->emailValidator->isValid('someone@gmail.com'));

    static::assertSame(
      static::VALID_RESPONSE_DATA,
      $this->emailValidator->getAllValidationMetadata('someone@gmail.com')
    );

  }

  /**
   * Test case for an invalid email address.
   */
  public function testEmailValidatorInvalid() {

    $invalid_response = APIResponse::ofData(
      200,
      'json',
      static::INVALID_RESPONSE_DATA
    );
    $this->mockClient->setApiResponse($invalid_response);

    static::assertFalse($this->emailValidator->isValid('invalid-syntax-test'));

    static::assertSame(
      static::INVALID_RESPONSE_DATA,
      $this->emailValidator->getAllValidationMetadata('invalid-syntax-test')
    );

  }

  /**
   * Test case for an API error condition.
   */
  public function testEmailValidatorApiError() {
    $api_error_response = APIResponse::ofErrorCause(
      777,
      'Mock error'
    );
    $this->mockClient->setApiResponse($api_error_response);

    // Should fall back to the decorated email validator.
    static::assertSame(
      static::FALLBACK_VALID_DATA,
      $this->emailValidator->getAllValidationMetadata('someone@gmail.com')
    );

    static::assertSame(
      static::FALLBACK_INVALID_DATA,
      $this->emailValidator->getAllValidationMetadata('invalid-syntax-test')
    );
  }

}
