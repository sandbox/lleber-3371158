<?php

namespace Drupal\neutrino_api_ip_info;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use NeutrinoAPI\NeutrinoAPIClient;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * This service taps the Neutrino API Ip Info service.
 */
class NeutrinoIpInfo implements NeutrinoIpInfoInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The cache factory service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The client factory service.
   *
   * @var \NeutrinoAPI\NeutrinoAPIClient
   */
  protected $client;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $requestStack;

  /**
   * Creates a neutrino ip info service instance.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \NeutrinoAPI\NeutrinoAPIClient $client
   *   The client factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   */
  public function __construct(CacheBackendInterface $cache, TimeInterface $time, LoggerChannelInterface $logger, NeutrinoAPIClient $client, RequestStack $request_stack) {
    $this->cache = $cache;
    $this->time = $time;
    $this->logger = $logger;
    $this->client = $client;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function getIpInfoMetadata($ip = NULL) {
    if (!$ip) {
      // No IP was provided. Defaulting to current request's IP.
      $ip = $this->requestStack->getCurrentRequest()->getClientIp();
    }

    if ($this->cache->get($ip) === FALSE) {
      $response = $this->client->ipInfo(['ip' => $ip]);
      if ($response->isOK()) {
        // Discuss timing for this cache.
        $this->cache->set($ip, $response->getData(), $this->time->getCurrentTime() + 60);
      }
      else {
        $this->logger->error($response->getErrorMessage());
      }
    }

    return $this->cache->get($ip)->data ?? [];
  }

}
