<?php

namespace Drupal\neutrino_api_ip_info;

/**
 * Provides an interface for the Neutrino API ip info service.
 *
 * @see https://www.neutrinoapi.com/api/ip-info/
 */
interface NeutrinoIpInfoInterface {

  /**
   * Gets an array of neutrino ip info metadata.
   *
   * @param string $ip
   *   The ip address to get ip info metadata for.
   *
   * @return array
   *   An associative array of metadata, or an empty array if an error occurs.
   */
  public function getIpInfoMetadata($ip);

}
