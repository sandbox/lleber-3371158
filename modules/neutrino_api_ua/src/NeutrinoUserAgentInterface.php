<?php

namespace Drupal\neutrino_api_ua;

/**
 * Provides an interface for the Neutrino API User Agent lookup service.
 *
 * @see https://www.neutrinoapi.com/api/ua-lookup/
 */
interface NeutrinoUserAgentInterface {

  /**
   * Gets an array of neutrino user agent metadata.
   *
   * @param string $ua
   *   The client's user agent to get user agent metadata for.
   *
   * @return array
   *   An associative array of metadata.
   */
  public function getUserAgentMetadata($ua);

}
