<?php

namespace Drupal\neutrino_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for the neutrino module.
 */
class NeutrinoSettingsForm extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['neutrino_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neutrino_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('neutrino_api.settings');

    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('user_id'),
    ];
    $form['api_key'] = [
      '#type' => 'password',
      '#title' => $this->t('API Key'),
    ];

    if ($this->moduleHandler->moduleExists('key')) {

      $overrides = $this->entityTypeManager->getStorage('key_config_override')->loadMultiple();
      foreach ($overrides as $override) {
        /** @var \Drupal\key\KeyConfigOverrideInterface $override */

        if ($override->getConfigName() === 'neutrino_api.settings' && $override->getConfigItem() === 'api_key') {
          $form['api_key']['#disabled'] = TRUE;
          $form['api_key']['#description'] = $this->t('This value is managed by the <em>key</em> module and cannot be configured from here.  <a href="@keys_url">Check your keys</a> for more information.', [
            '@keys_url' => $override->toUrl('collection')->toString(),
          ]);
          break;
        }
      }
    }

    if (!empty($config->get('api_key'))) {
      $form['api_key']['#description'] = $this->t('An API key is already configured. Adding a new API key here will overwrite the old value.');
    }
    else {
      $form['api_key']['#required'] = TRUE;
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('neutrino_api.settings');

    $config->set('user_id', $form_state->getValue('user_id'));

    // Only set an API key if the user provided one.
    $api_key = $form_state->getValue('api_key');
    if (!empty($api_key)) {
      $config->set('api_key', $api_key);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
