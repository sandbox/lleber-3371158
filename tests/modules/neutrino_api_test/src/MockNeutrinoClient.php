<?php

namespace Drupal\neutrino_api_test;

use NeutrinoAPI\APIResponse;
use NeutrinoAPI\NeutrinoAPIClient;

/**
 * An offline mock of the Neutrino client.
 */
class MockNeutrinoClient extends NeutrinoAPIClient {

  /**
   * A mock API response.
   *
   * @var \NeutrinoAPI\APIResponse
   */
  protected $apiResponse;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('dummy_username', 'dummy_password');
    $this->apiResponse = APIResponse::ofData(
      200,
      'text/json', [
        'valid' => TRUE,
      ]
    );
  }

  /**
   * Sets a known API response (useful for blackbox testing).
   *
   * @param \NeutrinoAPI\APIResponse $api_response
   *   The API response to set.
   */
  public function setApiResponse(APIResponse $api_response) {
    $this->apiResponse = $api_response;
  }

  /**
   * {@inheritdoc}
   *
   * This method always returns the API response set with ::setApiResponse.
   */
  public function execRequest(string $httpMethod, string $endpoint, array $params, ?string $outputFilePath, int $readTimeoutInSeconds): APIResponse {
    return $this->apiResponse;
  }

}
