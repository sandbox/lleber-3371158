<?php

namespace Drupal\neutrino_api_test;

use Drupal\neutrino_api\NeutrinoClientFactoryInterface;

/**
 * A client factory that produces mocks.
 */
class MockNeutrinoClientFactory implements NeutrinoClientFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create() {
    return new MockNeutrinoClient();
  }

}
